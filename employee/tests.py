from django.test import TestCase, Client
from .models import Employee
import json


class EmployeeTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.tupac = Employee(name="Tupac Shakur",
                             email="tupac.shakur@luizalabs.com",
                             department="compliance")
        cls.tupac.save()
        cls.authorization_header = "Basic YWRtaW46YWRtaW4="
        cls.content_type_header = "application/json"

    def test_unauthorized_resource(self):
        error = self.assert_unauthorized(
            self.client.get('/employee/', follow=True))

        self.assertEqual(error, {
            "detail": "Authentication credentials were not provided."
        })

    def test_get_employees(self):
        employees = self.get('/employee/')

        self.assertEqual(employees, [{
            "id": 1,
            "name": "Tupac Shakur",
            "email": "tupac.shakur@luizalabs.com",
            "department": "compliance"}])

    def test_get_employee(self):
        employee = self.get('/employee/1')

        self.assertEqual(employee, {
            "id": 1,
            "name": "Tupac Shakur",
            "email": "tupac.shakur@luizalabs.com",
            "department": "compliance"})

    def test_get_notfound_employee(self):
        error = self.get('/employee/10000/', assert_fn='assert_not_found')

        self.assertEqual(error, {
            "detail": "Not found."
        })

    def test_save_employee(self):
        employee = self.post('/employee/', {
            "name": "Riley Ben King",
            "email": "b.b.king@luizalabs.com",
            "department": "finance"})

        id = employee['id']

        self.assertEqual(self.get('/employee/{0}/'.format(id)), {
            "id": id,
            "name": "Riley Ben King",
            "email": "b.b.king@luizalabs.com",
            "department": "finance"})

    def test_name_required(self):
        error = self.post('/employee/',
                          {"email": "b.b.king@other.com",
                           "department": "finance"},
                          assert_fn='assert_bad_request')

        self.assertEqual(error, {'name': ['This field is required.']})

    def test_email_required(self):
        error = self.post('/employee/',
                          {"name": "Riley Ben King",
                         "department": "finance"},
                          assert_fn = 'assert_bad_request')

        self.assertEqual(error, {'email': ['This field is required.']})

    def test_department_required(self):
        error=self.post('/employee/',
                          {"name": "Riley Ben King",
                           "email": "b.b.king@other.com"},
                          assert_fn = 'assert_bad_request')

        self.assertEqual(error, {'department': ['This field is required.']})

    def test_email_validation(self):
        error=self.post('/employee/',
                          {"name": "Riley Ben King",
                           "email": "b.b.king#other.com",
                           "department": "finance"},
                          assert_fn = 'assert_bad_request')

        self.assertEqual(error, {'email': ['Enter a valid email address.']})

    def test_update_employee(self):
        self.put('/employee/1/', {
            "name": "Tupac Shakur",
            "email": "tupac.shakur@luizalabs.com",
            "department": "ethics"})

        self.assertEqual(self.get('/employee/1/'), {
            "id": 1,
            "name": "Tupac Shakur",
            "email": "tupac.shakur@luizalabs.com",
            "department": "ethics"})

    def test_partial_update_employee(self):
        self.patch('/employee/1/', {"department": "ethics"})

        self.assertEqual(self.get('/employee/1/'), {
            "id": 1,
            "name": "Tupac Shakur",
            "email": "tupac.shakur@luizalabs.com",
            "department": "ethics"})

    def test_partial_update_employee_not_found(self):
        error=self.patch('/employee/100000/',
                           {"department": "ethics"}, assert_fn = 'assert_not_found')

        self.assertEqual(error, {
            "detail": "Not found."
        })

    def test_delete_employee(self):
        self.delete('/employee/1/')

        employees=self.get('/employee/1', assert_fn = 'assert_not_found')
        self.assertEqual(employees, {
            "detail": "Not found."
        })

    def test_delete_notfound_employee(self):
        error=self.delete('/employee/100000/', assert_fn = 'assert_not_found')
        self.assertEqual(error, {
            "detail": "Not found."
        })

    def delete(self, resource, assert_fn = 'assert_no_content'):
        """
        perform DELETE
        :return dict data
        """
        response=self.client.delete(resource,
                                      HTTP_AUTHORIZATION = self.authorization_header)

        return getattr(self, assert_fn)(response)

    def get(self, resource, assert_fn = 'assert_ok'):
        """
        perform GET
        :return dict data
        """
        response=self.client.get(resource,
                                   follow=True,
                                   HTTP_AUTHORIZATION=self.authorization_header)

        return getattr(self, assert_fn)(response)

    def patch(self, resource, obj, assert_fn='assert_ok'):
        """
        perform PATCH
        :return dict data
        """
        response = self.client.patch(resource,
                                     json.dumps(obj),
                                     content_type=self.content_type_header,
                                     HTTP_AUTHORIZATION=self.authorization_header)

        return getattr(self, assert_fn)(response)

    def put(self, resource, obj, assert_fn='assert_ok'):
        """
        perform PUT
        :return dict data
        """
        response = self.client.put(resource,
                                   json.dumps(obj),
                                   content_type=self.content_type_header,
                                   HTTP_AUTHORIZATION=self.authorization_header)

        return getattr(self, assert_fn)(response)

    def post(self, resource, obj, assert_fn='assert_created'):
        """
        perform POST
        :return dict data
        """
        response = self.client.post(resource,
                                    json.dumps(obj),
                                    content_type=self.content_type_header,
                                    HTTP_AUTHORIZATION=self.authorization_header)

        return getattr(self, assert_fn)(response)

    def assert_ok(self, response):
        self.assertEqual(response.status_code, 200)
        return json.loads(response.content.decode('utf-8'))

    def assert_not_found(self, response):
        self.assertEqual(response.status_code, 404)
        return json.loads(response.content.decode('utf-8'))

    def assert_bad_request(self, response):
        self.assertEqual(response.status_code, 400)
        return json.loads(response.content.decode('utf-8'))

    def assert_no_content(self, response):
        self.assertEqual(response.status_code, 204)
        body = response.content.decode('utf-8')
        return json.loads(body) if body else body

    def assert_created(self, response):
        self.assertEqual(response.status_code, 201)
        return json.loads(response.content.decode('utf-8'))

    def assert_unauthorized(self, response):
        self.assertEqual(response.status_code, 401)
        return json.loads(response.content.decode('utf-8'))
