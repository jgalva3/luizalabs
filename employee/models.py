from django.db import models

class Employee(models.Model):

  name = models.CharField(max_length=200)
  email = models.EmailField(max_length=200)
  department = models.CharField(max_length=40)

  def __str__(self):
    return "{0} - {1}".format(self.id, self.name)

