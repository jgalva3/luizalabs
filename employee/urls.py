from django.conf.urls import url
from django.contrib import admin
from .views import employee_resource

from rest_framework import routers

urlpatterns = [
    url(r'/', employee_resource)
]
