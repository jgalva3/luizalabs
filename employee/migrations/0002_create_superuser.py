from __future__ import unicode_literals

from django.db import migrations, models
from django.contrib.auth.models import User


def create_admin_user(apps, schema_editor):
    User.objects.create_superuser('admin', 'admin@luizalabs.com', 'admin')


class Migration(migrations.Migration):

    initial = False

    dependencies = [        
        ('employee', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_admin_user),
    ]