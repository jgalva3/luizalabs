from django.shortcuts import redirect

def docs(request):
    return redirect('/doc/index.html')    