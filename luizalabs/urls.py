from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles import views

from rest_framework import routers

from luizalabs import settings
from employee.views import EmployeeViewSet
from .views import docs

router = routers.DefaultRouter()
router.register(r'employee', EmployeeViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', docs),
    url(r'^doc$', docs),
    url(r'^doc/(?P<path>.*)$', views.serve),
    url(r'^system-status/', include('health_check.urls')),
    url(r'^', include(router.urls)),
]
