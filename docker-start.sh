#!/bin/bash

export APP_USER="admin"
export APP_PASSWORD="admin"

python manage.py migrate

echo "=#================================================"
echo ""
echo ""
echo "   Django superuser --------------------| "
echo ""
echo "       user: admin"
echo "       password: admin "
echo ""
echo "       Swagger: http://127.0.0.1:8000/doc"      
echo "       Django Admin: http://127.0.0.1:8000/admin"      
echo "       System Status: http://127.0.0.1:8000/system-status/"   
echo ""   
echo "================================================#="

gunicorn -k tornado -b 0.0.0.0:8000 luizalabs.wsgi