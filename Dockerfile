FROM python:3.6.0-alpine

ADD . /opt/luizalabs
WORKDIR /opt/luizalabs

ENV STATIC_ROOT /opt/static/
ENV DB_FILE /opt/db/db.sqlite3

RUN pip install -r requirements.txt \
  && pip install -r requirements-docker.txt \  
  && mkdir -p /opt/static \
  && mkdir -p /opt/db \
  && python manage.py collectstatic --noinput \
  && python manage.py test -v 2 

CMD ["sh", "docker-start.sh"]