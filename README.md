# Luizalabs Employee Manager

Basic skill test at Django Framework

Requirements:

[Python 3.5+](https://www.python.org/ftp/python/3.6.0/python-3.6.0-macosx10.6.pkg)
  
[Docker 1.13.0 (optional)](https://www.docker.com/products/overview)

# Deliverables

Swagger UI: 

[http://127.0.0.1:8000/doc](http://127.0.0.1:8000/doc)

Django Admin: 

[http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)

System status:

[http://127.0.0.1:8000/system-status/](http://127.0.0.1:8000/system-status/)

Resource Employee
 
`GET /employee/`

`GET /employee/{id}/`

`POST /employee/`

`PUT /employee/{id}/`

`PATCH /employee/{id}/`

`DELETE /employee/{id}/`

Unit Tests: 

`employee/tests.py`

Swagger definition:

`employee/static/swagger.yml`

# How to test

## With Docker (best)

Build image

`docker build -t luizalabs/employee .`

Run container

`docker run -p 8000:8000 -v $(pwd):/opt/db  -it luizalabs/employee`

Swagger UI: 

[http://127.0.0.1:8000/doc](http://127.0.0.1:8000/doc)

Django Admin: 

[http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/)

User: admin

Password: admin

System status:

[http://127.0.0.1:8000/system-status/](http://127.0.0.1:8000/system-status/)

## Or With Python 3.5+

Install requirements

`pip install -r requirements.txt`

Run tests

`python manage.py test -v 2`

Run migrations

`python manage.py migrate`

Run application

`python manage.py runserver`

Swagger UI: 

[http://127.0.0.1:8000/doc](http://127.0.0.1:8000/doc)

Django Admin: 

[http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)

System status:

[http://127.0.0.1:8000/system-status/](http://127.0.0.1:8000/system-status/)

# cUrl examples

## Create
```
curl -vX POST -H "Content-Type: application/json" -H "Authorization: Basic YWRtaW46YWRtaW4=" -d '{
 "name": "Don Vito Corleone",
 "email": "godfather@luizalabs.com",
 "department": "sales"
 }' "http://127.0.0.1:8000/employee/"
```

## Get all

```
curl -vX GET -H "Authorization: Basic YWRtaW46YWRtaW4=" "http://127.0.0.1:8000/employee/"
```

## Get by id

```
curl -vX GET -H "Authorization: Basic YWRtaW46YWRtaW4=" "http://127.0.0.1:8000/employee/1/"
```

## Update
```
curl -vX PUT -H "Content-Type: application/json" -H "Authorization: Basic YWRtaW46YWRtaW4=" -d '{
 "name": "Don Vito Corleone",
 "email": "godfather@luizalabs.com",
 "department": "finance"
 }' "http://127.0.0.1:8000/employee/1/"
```

## Partial Update

```
curl -vX PATCH -H "Content-Type: application/json" -H "Authorization: Basic YWRtaW46YWRtaW4=" -d '{
 "name": "Michael Corleone" 
 }' "http://127.0.0.1:8000/employee/1/"
```

## Delete

```
curl -vX DELETE -H "Authorization: Basic YWRtaW46YWRtaW4=" "http://127.0.0.1:8000/employee/1/"
```
